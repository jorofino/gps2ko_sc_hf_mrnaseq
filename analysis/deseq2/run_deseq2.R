library(docopt)
library(DESeq2)

'Usage: run_deseq2.R <count_matrix>' -> doc
opts <- docopt(doc, commandArgs(trailingOnly = TRUE))

#initialize matrix of counts 
counts <- read.table(opts$count_matrix, header=TRUE, row.names='gene_id')
counts <- as.matrix(counts)
storage.mode(counts) <- 'integer'

#make coldata df
samplenames <- colnames(counts)
conditions <- c(rep('KO', 1), rep('WT', 2), rep('KO', 1), rep('WT', 1), rep('KO', 2))
conditions <- as.factor(conditions)
litter <- c(rep('L1', 2), rep('L2', 2), rep('L3', 3))
coldata <- data.frame(samplenames, conditions, litter)

#run deseq from matrix for model with litter cov
dds <- DESeqDataSetFromMatrix(countData = counts, colData = coldata, design = ~ litter + conditions)
dds$conditions <- relevel(dds$conditions, ref='WT')
dds <- dds[ rowMeans(counts(dds)) > 10, ]

#construct file names
typeres <- unlist(strsplit(basename(opts$count_matrix), '_'))[2]
deseqout <- paste(typeres, 'deseq2', 'de', 'tsv', sep = '.')
rldname <- paste(typeres, 'rld', 'RDS', sep = '.')

#DESeq analysis
dds <- DESeq(dds)
res <- results(dds, contrast = c('conditions', 'KO', 'WT'))
resOrdered <- res[order(res$padj),]
write.table(as.data.frame(resOrdered), file=file.path('samples', deseqout), sep='\t', quote=FALSE)  

#write out regularized log transform
rld <- rlog(dds, blind=FALSE)
saveRDS(rld, file=file.path('samples', rldname))

#extracting normalized counts
dds <- estimateSizeFactors(dds)
norm.counts <- counts(dds, normalized=TRUE)
name <- paste('norm', typeres, 'matrix', 'tsv', sep='.')
colnames(norm.counts) <- samplenames
write.table(norm.counts, file=file.path('samples', name), sep='\t', col.names=TRUE, quote=FALSE)
